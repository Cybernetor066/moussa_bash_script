#!/usr/bin/bash
# script name: automate_health_check.sh
# Login to one account

declare -a my_arr=("AKIAYJPWE4DTCCNCDDPU:Bic4zeA:us-east1:json" "AKIAXZGQTO3ZFDRYHVWS:bA2AKtquiSzsluG52YJf8q/r2aJIaLnJilGAE+iX:us-east-1:json" "AKIAY6KZGRLUECAZYI63:R6R9mIcEYXKyWCoqu6qri/Et7I5J1u4amxZxHjPr:us-east-2:json")

for item in "${my_arr[@]}"; do
    IFS=':'
    read -a strarr <<< "$item"
    # echo "The line with $item will be split shortly"

    # Using it to set variables
    ACCESS_KEY_ID="${strarr[0]}"
    SECRET_ACCESS_KEY="${strarr[1]}"
    AWS_REGION="${strarr[2]}"
    AWS_OUTPUT_FORMAT="${strarr[3]}"


    # echo $ACCESS_KEY_ID
    # echo $SECRET_ACCESS_KEY
    # echo $AWS_REGION
    # echo $AWS_OUTPUT_FORMAT

    
    aws configure set aws_access_key_id "${ACCESS_KEY_ID}"
    aws configure set aws_secret_access_key "${SECRET_ACCESS_KEY}"
    aws configure set region "${AWS_REGION}"
    aws configure set output "${AWS_OUTPUT_FORMAT}"


    # Real request
    echo "Target Group health check status for all target groups in the account with id: ${ACCESS_KEY_ID}"
    aws elbv2 describe-target-groups | egrep 'TargetGroupName|HealthCheckEnabled' > $ACCESS_KEY_ID-heath_checks


done














